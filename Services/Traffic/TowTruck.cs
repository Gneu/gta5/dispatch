﻿namespace Dispatch.Services.Traffic
{
	using System.Drawing;
	using Rage;

	public class TowTruckJob : ServiceVehicleJob
	{
		public enum TowTruckClass : uint
		{
			Light = 3852654278u,
			Medium = 2971866336u,
			Heavy = 1353720154u
		}

		public TowTruckJob(Ped localPlayerCharacter, TowTruckClass classification) : base(localPlayerCharacter)
		{
			VehicleType = (uint)classification;

			BlipColor = Color.Yellow;
			BlipIcon = BlipSprite.Garage;

			TargetPed = localPlayerCharacter;

			SpawnVehicle();
		}

		public void AttachVehicle(Vehicle vehicle)
		{
			DriveToEntity(vehicle, 5, MathHelper.ConvertKilometersPerHourToMetersPerSecond(20), VehicleDrivingFlags.Emergency);

			Game.DisplaySubtitle("Towing Vehicle");

			vehicle.Position = _vehicle.RearPosition;
			vehicle.Rotation = _vehicle.Rotation;

			if (_vehicle.HasTowArm)
			{
				Rage.Native.NativeFunction.CallByHash<uint>(0xFE54B92A344583CA, _vehicle, 1.0f);
			}
			else
			{
				// TODO: Confirm that this is what we do for the flatbed. =\
				_vehicle.Trailer = vehicle;
			}

			DriveToEntity(_vehicle, 10, MathHelper.ConvertKilometersPerHourToMetersPerSecond(20));
		}
	}
}
