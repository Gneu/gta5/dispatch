﻿namespace Dispatch.Services
{
	using System;
	using System.Collections.Generic;
	using System.Drawing;
	using System.Linq;
	using Rage;
	using Rage.Native;

	public abstract class ServiceVehicleJob
	{
		protected Ped _driver;
		protected Vehicle _vehicle;

		public uint VehicleType { get; set; }

		public Ped TargetPed { get; set; }
		public Color BlipColor { get; set; }
		public Blip VehicleBlip { get; set; }

		public BlipSprite BlipIcon { get; set; }

		public List<Entity> Entities { get; set; }

		protected ServiceVehicleJob(Ped targetPed)
		{
			TargetPed = targetPed;
			BlipColor = Color.White;

			Entities = new List<Entity>();
		}

		public void SpawnVehicle()
		{
			float heading = 0f;
			Vector3 spawnPoint;

			if (!TargetPed.IsValid())
			{
				throw new Exception("TargetPed not set!");
			}

			var target = World.GetNextPositionOnStreet(TargetPed.Position.Around2D(200));
			var playerPosition = TargetPed.Position;

			_driver = new Ped(new Model("a_m_m_hillbilly_01"), target, heading);
			_vehicle = new Vehicle(new Model(VehicleType), target, heading);

			if (!_driver.IsValid())
			{
				throw new Exception("Driver not created!");
			}

			if (!_vehicle.IsValid())
			{
				throw new Exception("Vehicle not created!");
			}

			NativeFunction.Natives.GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION<bool>(target.X, target.Y, target.Z,
				playerPosition.X, playerPosition.Y, playerPosition.Z, 0, out spawnPoint, out heading, 0, 0x40400000, 0);

			_driver.WarpIntoVehicle(_vehicle, -1);

			SetDrivingStyle();
			CreateBlip();

			Entities.Add(_driver);
			Entities.Add(_vehicle);
		}

		private void CreateBlip()
		{
			Game.LogTrivial($"{_vehicle}");

			// Remove all other blips
			foreach (var attachedBlip in _vehicle.GetAttachedBlips())
			{
				attachedBlip.Delete();
			}

			VehicleBlip = _vehicle.AttachBlip();

			VehicleBlip.Sprite = BlipIcon;
			VehicleBlip.Color = BlipColor;
			VehicleBlip.Scale = 0.625f;
		}

		private void SetDrivingStyle()
		{
			NativeFunction.Natives.SET_DRIVE_TASK_DRIVING_STYLE(_driver, 786607);
			NativeFunction.Natives.SET_DRIVER_AGGRESSIVENESS(_driver, 0f);
			NativeFunction.Natives.SET_DRIVER_ABILITY(_driver, 1f);
		}

		public void DriveToTargetPed(float distance = 5f, float maxSpeed = 60f, VehicleDrivingFlags flags = VehicleDrivingFlags.Normal)
		{
			DriveToEntity(TargetPed, distance, maxSpeed, flags);
		}

		public void DriveToEntity(Entity entity, float distance = 5f, float maxSpeed = 60f, VehicleDrivingFlags flags = VehicleDrivingFlags.Normal)
		{
			Vector3 nearestOnStreet = World.GetNextPositionOnStreet(entity.Position);

			Game.LogTrivial(("Moving to player"));

			var task = _driver.Tasks.DriveToPosition(nearestOnStreet,
					MathHelper.ConvertKilometersPerHourToMetersPerSecond(maxSpeed),
					flags, Vector3.Distance(entity.Position, nearestOnStreet) + distance);

			GameFiber.WaitWhile(() => task.Status == TaskStatus.InProgress || task.Status == TaskStatus.Preparing);

			// TODO: Handle TaskStatus.Interrupted
		}

		public void Dismiss()
		{
			_driver.Dismiss();
			_vehicle.Dismiss();

			if (VehicleBlip)
			{
				VehicleBlip.Delete();
			}

			Entities.ForEach(entity =>
			{
				entity.Dismiss();
			});
		}
	}
}
