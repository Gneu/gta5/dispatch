﻿namespace Dispatch
{
	using LSPD_First_Response.Mod.API;
	using Rage;
	using System.IO;

	public class EntryPoint : Plugin
	{
		private static bool _isOnDuty;
		private static bool _isRunning;

		private static bool DependenciesFound
		{
			get
			{
				var success = true;

				if (!File.Exists("RAGENativeUI.dll"))
				{
					Game.LogTrivial("[Dispatch] Rage Native UI Not found.");
					success = false;
				}
				else
				{
					Game.LogVerbose("[Dispatch] Rage Native UI found.");
				}

				return success;
			}
		}

		public static void InitializeLatest()
		{
			Game.LogTrivial($"[Dispatch] {AssemblyUtilities.GameStatus} v.{AssemblyUtilities.Version}");
			Game.LogTrivial(
				$"[Dispatch] This was last built {AssemblyUtilities.BuildTimestamp} #{AssemblyUtilities.BuildNumber}/{AssemblyUtilities.BuildTag}");

			if (!DependenciesFound)
			{
				Game.DisplayNotification("3dtextures", "mpgroundlogo_cops", "Dispatch Initialization", "~r~Failed",
					"Could not complete Initialization - Dependencies Not Found. Please share the logs with the development team.");
				Game.LogTrivial($"[Dispatch] Dependencies Not Found\n");

				return;
			}

			_isRunning = true;

			Functions.OnOnDutyStateChanged += duty =>
			{
				_isOnDuty = duty;
			};

			GameFiber.StartNew(() =>
			{
				using (var radio = new DispatchRadio())
				{
					while (_isRunning)
					{
						if (!_isOnDuty)
						{
							return;
						}

						radio.HandleInput();
						radio.Tick();

						GameFiber.Yield();
					}
				}
			});
		}

		public static void ReportPossibleIncompatibility()
		{
			Game.LogTrivial("[Dispatch] You are running a version of the application that we may not be compatible with");
			Game.LogTrivial("[Dispatch] Please let us know of any issues you run into, so we can best maintain this plugin");
			Game.LogTrivial("");
			Game.LogTrivial($"[Dispatch] Game ProductVersion: {Game.ProductVersion}");
			Game.LogTrivial($"[Dispatch] Game Build Number:   {Game.BuildNumber}");
		}

		public override void Initialize()
		{
			switch (Game.BuildNumber)
			{
				case 1604:
					InitializeLatest();
					break;

				default:
					ReportPossibleIncompatibility();
					InitializeLatest();
					break;
			}
		}

		public override void Finally()
		{
			_isRunning = false;
			_isOnDuty = false;
		}
	}
}
