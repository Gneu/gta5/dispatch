﻿namespace Dispatch
{
	using System;
	using System.Reflection;

	public class AssemblyUtilities
	{
		private static string _gameStatus;
		private static Version _version;

		public static string BuildNumber
		{
			get
			{
				if (string.IsNullOrEmpty(BuildInfo.BuildNumber))
				{
					return "Local";
				}

				return BuildInfo.BuildNumber;
			}
		}

		public static string BuildTag
		{
			get
			{
				if (string.IsNullOrEmpty(BuildInfo.BuildTag))
				{
					return "LOCAL";
				}

				return BuildInfo.BuildTag;
			}
		}

		public static string BuildTimestamp
		{
			get
			{
				if (string.IsNullOrEmpty(BuildInfo.BuildTimestamp))
				{
					return "Local";
				}

				return BuildInfo.BuildTimestamp;
			}
		}

		public static string GameStatus
		{
			get
			{
				if (_gameStatus != null)
				{
					return _gameStatus;
				}

				var desc =
					Assembly.GetAssembly(typeof(AssemblyUtilities)).GetCustomAttributes(typeof(AssemblyConfigurationAttribute), true)[0
					] as AssemblyConfigurationAttribute;

				if (desc != null && desc.Configuration.Equals("Release"))
				{
					_gameStatus = "Public";
				}
				else
				{
					_gameStatus = "Internal";
				}

				return _gameStatus;
			}
		}

		public static Version Version
			=> _version ?? (_version = Assembly.GetAssembly(typeof(AssemblyUtilities)).GetName().Version);
	}
}
