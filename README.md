# Dispatch

This is an immersion plugin for LSPDFR.

## Setup

The best place to grab the latest version of the plugin is the [Releases](https://gitlab.com/Gneu/gta5/dispatch/releases) listing.
