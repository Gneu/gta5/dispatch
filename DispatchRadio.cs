﻿namespace Dispatch
{
	using System;
	using System.Collections.Generic;
	using Menus;
	using Rage;
	using RAGENativeUI;
	using RAGENativeUI.Elements;
	using Services.Traffic;

	public class DispatchRadio : IDisposable
	{
		private readonly UIMenuSwitchMenusItem _menuSwitcher;

		private Player LocalPlayer => Game.LocalPlayer;

		private readonly List<Entity> _entities;

		public DispatchRadio()
		{
			_entities = new List<Entity>();

			Game.DisplayNotification("3dtextures", "mpgroundlogo_cops", "Dispatch Initialization", "~b~Completed",
				"Hit your phone button for the radio controls");

			MenuPool = new MenuPool();
				
			// Create our menus
			var menu1 = CreateMenu("~b~Dispatch", "Panic");
			// var menu2 = CreateMenu("~r~Emergency", "EMS", "Fire", "Coroner");
			var menu3 = TrafficMenu.Create(); //  CreateMenu("~g~Traffic", "Flatbed", "Medium", "Light");
											  // var menu4 = CreateMenu("~w~Support", "Marked", "Unmarked", "Female");
											  // var menu5 = CreateMenu("~y~Special", "SWAT", "NOOSE", "Air");

			MenuPool.Add(menu1);
			// MenuPool.Add(menu2);
			MenuPool.Add(menu3);

			// Create the menu switcher
			_menuSwitcher = new UIMenuSwitchMenusItem("Contact", "", new DisplayItem(menu1, "Dispatch"),
				//												 new DisplayItem(menu2, "EMS"),
																 new DisplayItem(menu3, "Traffic")
					//											 new DisplayItem(menu4, "Support"),
						//										 new DisplayItem(menu5, "Special")
						);

			menu1.AddItem(_menuSwitcher, 0);
			// menu2.AddItem(_menuSwitcher, 0);
			menu3.AddItem(_menuSwitcher, 0);
			// menu4.AddItem(_menuSwitcher, 0);
			// menu5.AddItem(_menuSwitcher, 0);

			menu1.RefreshIndex();
			// menu2.RefreshIndex();
			menu3.RefreshIndex();
			// menu4.RefreshIndex();
			// menu5.RefreshIndex();

			// Temporal fix to prevent some flickering that happens occasionally when switching menus
			_menuSwitcher.OnListChanged += (s, i) => { _menuSwitcher.CurrentMenu.Draw(); };
		}

		private UIMenu CreateMenu(string name, params string[] menusDisplayItems)
		{
			UIMenu m = new UIMenu(name, "");

			foreach (var entry in menusDisplayItems)
			{
				m.AddItem(new UIMenuItem(entry));
			}

			//m.OnItemSelect += MenuOnOnItemSelect;

			MenuPool.Add(m);

			return m;
		}

		public MenuPool MenuPool { get; set; }

		private void MenuOnOnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
		{
			Game.DisplaySubtitle("You have selected: " + sender.Title.Caption + " ~b~" + selectedItem.Text + " - " + selectedItem.Description, 500);

			switch (sender.Title.Caption)
			{
				case "~g~Traffic":

					switch (selectedItem.Text)
					{
						case "Flatbed":
							SpawnTowTruck(TowTruckJob.TowTruckClass.Heavy);
							break;

						case "Medium":
							SpawnTowTruck(TowTruckJob.TowTruckClass.Medium);
							break;

						case "Light":
							SpawnTowTruck(TowTruckJob.TowTruckClass.Light);
							break;
					}

					break;
			}
		}

		private void SpawnTowTruck(TowTruckJob.TowTruckClass truckType)
		{
			Game.LocalPlayer.Character.Tasks.PlayAnimation("random@arrests", "generic_radio_chatter", 1.5f, AnimationFlags.SecondaryTask | AnimationFlags.UpperBodyOnly);
			var targetVehicle = LocalPlayer.LastVehicle;

			GameFiber.StartNew(() =>
			{
				var towTruckJob = new TowTruckJob(LocalPlayer.Character, truckType);

				towTruckJob.DriveToTargetPed(100);

				towTruckJob.DriveToTargetPed(10, 20f, VehicleDrivingFlags.Emergency);

				towTruckJob.AttachVehicle(targetVehicle);

				towTruckJob.Dismiss();
			});
		}

		public void Dispose()
		{
			_entities.ForEach((entity) =>
			{
				entity.Dismiss();
				entity.Delete();
			});

			MenuPool.CloseAllMenus();
		}

		public void Tick()
		{
			MenuPool.ProcessMenus();
		}

		public void HandleInput()
		{
			if (Game.IsControlJustPressed(LocalPlayer.Index, GameControl.CellphoneUp))
			{
				_menuSwitcher.CurrentMenu.Visible = true;
			}
		}

		public void Track(Entity entity)
		{
			_entities.Add(entity);
		}
	}
}
