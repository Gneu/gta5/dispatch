﻿namespace Dispatch.Menus
{
	// http://gtaforums.com/topic/822314-guide-driving-styles/
	enum DrivingStyle : uint
	{
		StopForVehicles = 2 ^ 0,
		StopForPeds = 2 ^ 1,
		AvoidVehicles = 2 ^ 2,
		AvoidEmptyVehicles = 2 ^ 3,
		AvoidPeds = 2 ^ 4,
		AvoidObjects = 2 ^ 5,
		Unknown1 = 2 ^ 6,
		StopAtTrafficLights = 2 ^ 7,
		UseBlinkers = 2 ^ 8,
		AllowWrongWay = 2 ^ 9,
		GoInReverse = 2 ^ 10,
		Unknown2 = 2 ^ 11,
		Unknown3 = 2 ^ 12,
		Unknown4 = 2 ^ 13,
		Unknown5 = 2 ^ 14,
		Unknown6 = 2 ^ 15,
		Unknown7 = 2 ^ 16,
		Unknown8 = 2 ^ 17,
		ShortestPath = 2 ^ 18,
		AvoidOffroad = 2 ^ 19,
		Unknown9 = 2 ^ 20,
		Unknown10 = 2 ^ 21,
		IgnoreRoads = 2 ^ 22,
		Unknown11 = 2 ^ 23,
		IgnorePathingEntirely = 2 ^ 24,
		Unknown12 = 2 ^ 24,
		Unknown13 = 2 ^ 24,
		Unknown14 = 2 ^ 24,
		Unknown15 = 2 ^ 24,
		AvoidHighways = 2 ^ 24,
		Unknown16 = 2 ^ 24,

		Normal = 786603,
		AvoidTraffic = 786468,
		IgnoreLights = 2883621
	}
}
