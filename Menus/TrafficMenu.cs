﻿namespace Dispatch.Menus
{
	using System;
	using Rage;
	using RAGENativeUI;
	using RAGENativeUI.Elements;
	using Services.Traffic;

	class TrafficMenu : UIMenu
	{
		private UIMenuListItem _listItem;
		private UIMenuItem _dispatchItem;
		private Player LocalPlayer => Game.LocalPlayer;

		public static UIMenu Create()
		{
			var instance = new TrafficMenu();

			return instance;
		}

		public TrafficMenu() : base("~g~Traffic", "")
		{
			AddItem(_listItem = new UIMenuListItem("Class", "Select the class of the target", "Light", "Medium", "Heavy"));
			AddItem(_dispatchItem = new UIMenuItem("~r~Dispatch", "Send request along"));

			OnItemSelect += OnOnItemSelect;
		}

		private void OnOnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
		{
			if (selectedItem != _dispatchItem)
			{
				return;
			}

			if (LocalPlayer.Character.LastVehicle)
			{
				switch (_listItem.SelectedItem.DisplayText)
				{
					case "Heavy":
						SpawnTowTruck(TowTruckJob.TowTruckClass.Heavy);
						break;

					case "Medium":
						SpawnTowTruck(TowTruckJob.TowTruckClass.Medium);
						break;

					case "Light":
						SpawnTowTruck(TowTruckJob.TowTruckClass.Light);
						break;
				}

				Game.DisplayHelp($"You currently have {JobsInFlight} Tow jobs in progress");
				GoBack();
			}
			else
			{
				Game.DisplaySubtitle("Oops! I forgot to get into the vehicle =\\");
			}
		}

		private void SpawnTowTruck(TowTruckJob.TowTruckClass truckType)
		{
			Game.LocalPlayer.Character.Tasks.PlayAnimation("random@arrests", "generic_radio_chatter", 1.5f, AnimationFlags.SecondaryTask | AnimationFlags.UpperBodyOnly);
			var targetVehicle = LocalPlayer.LastVehicle;

			targetVehicle.MakePersistent();

			GameFiber.StartNew(() =>
			{
				try
				{
					JobsInFlight++;

					var towTruckJob = new TowTruckJob(LocalPlayer.Character, truckType);

					towTruckJob.DriveToTargetPed(100);
					towTruckJob.DriveToTargetPed(10, 20f, VehicleDrivingFlags.Emergency);

					towTruckJob.AttachVehicle(targetVehicle);

					towTruckJob.Dismiss();
				}
				finally
				{
					JobsInFlight--;
				}
			}, "Tow Truck Job");
		}

		public static int JobsInFlight { get; set; }
	}
}
